import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface XpTableItem {
  name: string;
  id: number;
  context: string;
  methodologie: string;
  equipe: string;
  realisation: string;
  langage: string;
  environement: string;
  date: string;
  duree: number;
  route: string;
}

var kinomapName = "Kinomap";
var kinomapRoute = "kinomap";
var kinomapContect = "Prestation de service pour DomusVI Domicile et Cooper&Co";
var kinomapMethodologie = "SCRUM";
var kinomapEquipe = "2";
var kinomapRea = "Gestion documentaire<br>annuaire<br>suivis des réclamations";
var kinomapLang = "MySQL<br>PHP procédural & POO<br>Jquery";
var kinomapEnv = "Linux<br>Apache<br>Symfony (1an)<br>GitLab";
var kinomapDate = "09-06-2015";
var kinomapDuree = 36;

var esocleName = "Escole";
var esocleRoute = "esocle";
var esolceContect = "Auto entrepreneur pour réaliser une plateforme de suivi de la qualité en entreprise.";
var esolceMethodologie = "-";
var esolceEquipe = "1";
var esolceRea = "Annuaire géolocalisé";
var esolceLang = "MySQL<br>PHP POO<br>Jquery";
var esolceEnv = "Linux<br>Apache";
var esocleDate = "02-01-2015";
var esocleDuree = 6;

// TODO: replace this with real data from your application
const EXAMPLE_DATA: XpTableItem[] = [
  {
    id: 1, name: kinomapName, route:kinomapRoute,  context: kinomapContect,
    methodologie: kinomapMethodologie, equipe: kinomapEquipe,
    realisation: kinomapRea, langage: kinomapLang, environement:kinomapEnv,
    date: kinomapDate, duree: kinomapDuree
  },
  {
    id: 2, name: esocleName,  route:esocleRoute, context: esolceContect,
    methodologie: esolceMethodologie, equipe: esolceEquipe,
    realisation: esolceRea, langage: esolceLang, environement:esolceEnv,
    date: esocleDate, duree: esocleDuree
  }
];

/**
 * Data source for the XpTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class XpTableDataSource extends DataSource<XpTableItem> {
  data: XpTableItem[] = EXAMPLE_DATA;

  constructor(private paginator: MatPaginator, private sort: MatSort) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<XpTableItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    // Set the paginator's length
    this.paginator.length = this.data.length;

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: XpTableItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: XpTableItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        case 'equipe': return compare(+a.equipe, +b.equipe, isAsc);
        case 'environement': return compare(+a.environement, +b.environement, isAsc);
        case 'context': return compare(+a.context, +b.context, isAsc);
        case 'realisation': return compare(+a.realisation, +b.realisation, isAsc);
        case 'langage': return compare(+a.langage, +b.langage, isAsc);
        case 'methodologie': return compare(+a.methodologie, +b.methodologie, isAsc);
        case 'date': return compare(+a.date, +b.date, isAsc);
        case 'duree': return compare(+a.duree, +b.duree, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
