import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { XpTableDataSource } from './xp-table-datasource';

@Component({
  selector: 'app-xp-table',
  templateUrl: './xp-table.component.html',
  styleUrls: ['./xp-table.component.css'],
})
export class XpTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: XpTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns =
  [
    'name', 'date', 'duree', 'context', 'methodologie', 'equipe',
    'langage', 'environement', 'realisation', 
  ];

  ngOnInit() {
    this.dataSource = new XpTableDataSource(this.paginator, this.sort);
  }
}
