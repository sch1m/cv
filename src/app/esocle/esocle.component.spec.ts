import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsocleComponent } from './esocle.component';

describe('EsocleComponent', () => {
  let component: EsocleComponent;
  let fixture: ComponentFixture<EsocleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsocleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsocleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
