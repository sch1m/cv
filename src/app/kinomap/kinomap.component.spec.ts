import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KinomapComponent } from './kinomap.component';

describe('KinomapComponent', () => {
  let component: KinomapComponent;
  let fixture: ComponentFixture<KinomapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KinomapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KinomapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
