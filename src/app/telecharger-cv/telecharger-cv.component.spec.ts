import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelechargerCVComponent } from './telecharger-cv.component';

describe('TelechargerCVComponent', () => {
  let component: TelechargerCVComponent;
  let fixture: ComponentFixture<TelechargerCVComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelechargerCVComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelechargerCVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
