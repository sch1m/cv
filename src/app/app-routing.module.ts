import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExperienceComponent } from './experience/experience.component';
import { FormationComponent } from './formation/formation.component';
import { TelechargerCVComponent } from './telecharger-cv/telecharger-cv.component';
import { KinomapComponent } from './kinomap/kinomap.component';
import { EsocleComponent } from './esocle/esocle.component';
import { CodeSourceComponent } from './code-source/code-source.component'

const routes: Routes = [
  { path: 'experiences', component: ExperienceComponent },
  { path: 'formations', component: FormationComponent },
  { path: 'telechargerCV', component: FormationComponent },
  { path: 'experiences/kinomap', component: KinomapComponent },
  { path: 'experiences/esocle', component: EsocleComponent },
  { path: 'sources', component: CodeSourceComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
