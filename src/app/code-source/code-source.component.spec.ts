import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeSourceComponent } from './code-source.component';

describe('CodeSourceComponent', () => {
  let component: CodeSourceComponent;
  let fixture: ComponentFixture<CodeSourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodeSourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
