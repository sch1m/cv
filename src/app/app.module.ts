import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormationComponent } from './formation/formation.component';
import { ExperienceComponent } from './experience/experience.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BaseComponent } from './base/base.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatPaginatorModule, MatSortModule, MatGridListModule, MatCardModule, MatMenuModule } from '@angular/material';
import { TelechargerCVComponent } from './telecharger-cv/telecharger-cv.component';
import {MatTableModule} from '@angular/material/table';
import { XpTableComponent } from './xp-table/xp-table.component';
import { KinomapComponent } from './kinomap/kinomap.component';
import { EsocleComponent } from './esocle/esocle.component';
import { FormationDBComponent } from './formation-db/formation-db.component';
import { CodeSourceComponent } from './code-source/code-source.component';

@NgModule({
  declarations: [
    AppComponent,
    FormationComponent,
    ExperienceComponent,
    BaseComponent,
    TelechargerCVComponent,
    XpTableComponent,
    KinomapComponent,
    EsocleComponent,
    FormationDBComponent,
    CodeSourceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
