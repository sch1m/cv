import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-formation-db',
  templateUrl: './formation-db.component.html',
  styleUrls: ['./formation-db.component.css'],
})
export class FormationDBComponent {



  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
        var cnamContent = '2010-2014';
        var escContent = '2001-2004';


      return [
        {
          title: 'CNAM - Licence informatrique Générale', cols: 1, rows: 2,
          content: cnamContent
        },
        {
          title: 'ESC Amiens - Master 2 Marketing', cols: 1, rows: 2,
          content: escContent
        }
      ];
    })
  );

  constructor(private breakpointObserver: BreakpointObserver) {}
}
